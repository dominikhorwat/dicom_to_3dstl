'''
Algorithm implemented in Python 3 to create printable, 3D STL model from CT images (cross-section). Algorithm uses Blender 2.78 to performs model postprocessing.


version 1.0 June 1, 2017


Oryginal Authors:

Dominik Horwat, MSc., Eng.
University of Gdańsk, Faculty of Mathematics, Physics and Informatics,
Institute of Theoretical Physics and Astrophysics,
Wit Stwosz 57, 80-952 Gdańsk, horwat.dominik@gmail.com

Marek Krośnicki, PhD, Eng.	
University of Gdańsk, Faculty of Mathematics, Physics and Informatics, Institute of Theoretical Physics and Astrophysics,
Wit Stwosz 57, 80-952 Gdańsk, fizmk@univ.gda.pl, http://iftia9.univ.gda.pl/~kroch


Requirements:
- Python 3
- Python modules: natsort, numpy, numpy-stl, pydicom, scipy (see Manual)
- Blender 2.78


Algorithm structure (modules):
I.	_1DICOMAnonymizer.py
II.	_2Sorting.py
III.	_3Preprocessing.py
IV.	_4MeshCreate.py
V.	_5PostprocessingSTL.py


Copyright 2017 Dominik Horwat, Marek Krośnicki


License: GPL2

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.


Description:
An example of using scripts for a 3D STL model reconstruction

IMPORTANT!!!
Before running please ensure that the following the conditions are met:
- The algorithm works only on CT cross-section. A folder with CT images must contain cross-section images,
but may also contain a different type of DICOM images (only cross-sections will be used).
- All scripts must be in single directory!
- Script names have not been changed
- There are no spaces between scans, scans overlap, otherwise, there will be gaps between layers in the 3d model. This is crucial for proper 3D reconstruction
- Spatial resolution (voxel size) should be high, for example 0.5mm, 1mm, 2mm (the algorithm was tested for voxel size 0.39x0.39x0.63mm)

WARNING!
- The algorithm is not optimized.
- For the recnstruction of big objects like skull at least 8GB RAM memory is required.

HOW TO USE:
1) Open STL_creating.py script
2) Specify a path to CT DICOM scans - dicomPath
3) Specify a path to the new folder for saving preprocessed images - savePath
4) Specify a path to the Blender 2.78 software (the path must include the Blender.exe) - 

Blender_path
5) Set a threshold value in the Hounsfield units for bone segmentation - threshold_HU
6) Set a volume threshold in voxels (by default volume_type=0) to filter small structures - 
threshold_volume (to set the volume threshold in mm3 set parameter volume_type=1)
7) Optionally - one can anonymize yours dicom files by uncomment the following code line: 
    #dicomPath = _1DICOMAnonymizer.anonymization(dicomPath, savePath)
8) Run this script

After running this script the DICOM files are first sorted/set in the correct position using the _2Sorting.py script.
(optionally: files are first anonimized using the _1DICOMAnonymizer.py script and then sorted/set in the correct position using the _2Sorting.py script

Next, images are segmented and filtered using threshold_HUs and threshold values parameters respectivelly, using the _3Preprocessing.py script.

Then, the 3D STL model is reconstructed using the _4MeshCreate.py script, that uses images preprocessed in the previous step.

After successful reconstruction, the model is automatically postprocessed in the Blender software. 
At the beginning, Blender is opened automatically without using the user interface (in console mode) and next the _5PostprocessingSTL.py script is executed (also automatically). During post-processing, the geometry of the model is cleaned and the model is smoothed.

After finishing the model processing, the final STL model is saved in a new folder in the same directory as preprocessed images (savePath). 
The 3D model is also saved in .blend format.

In case of problems with modules, install the following version of each module:
- numpy: 1.13.0
- scipy: 0.19.1
- natsort: 5.0.3
- pydicom: 0.9.9
- numpy-stl: 2.2.3
'''

import _1DICOMAnonymizer
import _2Sorting
import _3Preprocessing
import _4MeshCreate
import subprocess
import os



#########################################
#Specifying paths

#path to DICOM folder
dicomPath = "C:/.../.../<folder_with_dicoms>"
#e.g. dicomPath = "C:/User/DICOMS/<folder_with_dicom_images>"

#Save path for preprocessed images
savePath = "C:/.../.../<new_folder>"
#e.g. savePath = "C:/User/DICOMS/<new_folder_to_save_dicoms>"

#Path to Blender 2.78
Blender_path = 'C:/.../.../<blender_folder>/blender.exe'
#e.g. Blender_path = 'C:/User/Programs/Blender2.78/blender.exe'

########################################


########################################
#Setting preprocessing parameters

#Threshold value in the Hounsfield units (for bones 300HU is the lower limit) for images segmentation
threshold_HU = 300

#The threshold volume (in voxels or mm3) - objects with the volume greater than this threshold value are included into the model
#The choosing of the proper value depends on the size of the reconstructing object
threshold_volume = 10000 #in voxels
volume_type = 0 #in voxels, volume_type=1 for volume in mm3
########################################


########################################
#Running scripts

#DICOM anonimization
#dicomPath = _1DICOMAnonymizer.anonymization(dicomPath, savePath)

#Sorting images
_2Sorting.sorting(dicomPath, savePath)

#Preprocessing
output_pre = _3Preprocessing.preprocessing(savePath, threshold_HU, threshold_volume, volume_type)

#3D mesh building
output_stl = _4MeshCreate.create_mesh(output_pre[0], output_pre[1], output_pre[2], output_pre[3], output_pre[4], output_pre[5])

#Postprocessing
#Specifying path to script _5PostprocessingSTL
Script_path = os.getcwd() + '/_5PostprocessingSTL.py'
os.chdir(output_stl)
#Automated opening of the Blender software without GUI
p = subprocess.run([Blender_path, '-b', '-P', Script_path])

#Automated opening of the Blender software using GUI
#p = subprocess.run([Blender_path, '-P', Script_path])
#######################################
