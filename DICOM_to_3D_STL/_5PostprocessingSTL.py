'''
Algorithm implemented in Python 3 to create printable, 3D STL model from CT images (cross-section). Algorithm uses Blender 2.78 to perform model postprocessing.


version 1.0 June 1, 2017


Oryginal Authors:

Dominik Horwat, MSc., Eng.
University of Gdańsk, Faculty of Mathematics, Physics and Informatics,
Institute of Theoretical Physics and Astrophysics,
Wit Stwosz 57, 80-952 Gdańsk, horwat.dominik@gmail.com

Marek Krośnicki, PhD, Eng.	
University of Gdańsk, Faculty of Mathematics, Physics and Informatics, Institute of Theoretical Physics and Astrophysics,
Wit Stwosz 57, 80-952 Gdańsk, fizmk@univ.gda.pl, http://iftia9.univ.gda.pl/~kroch


Requirements:

- Python 3
- Python modules: natsort, numpy, numpy-stl, pydicom, scipy (see Manual)
- Blender 2.78


Algorithm structure (modules):
I.	_1DICOMAnonymizer.py
II.	_2Sorting.py
III.	_3Preprocessing.py
IV.	_4MeshCreate.py
V.	_5PostprocessingSTL.py


III. Preprocessing.py


Copyright 2017 Dominik Horwat, Marek Krośnicki


License: GPL2

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.


Description:

The function performs postprocessing of the CT images loaded from DICOM files
A smoothing level can be changed by setting the smooth_level value
'''

import bpy
import os

#####################
#Changing the smooth level
smooth_level = 6
smooth_factor = 0.7 #(0 to 1)
#####################

bpy.ops.wm.console_toggle()

#Import an stl model
_stl_path = os.getcwd()
print("Import STL file...")
temp_stl_path = _stl_path + '/Model_STL.stl'

bpy.ops.object.delete(use_global = False)

bpy.ops.import_mesh.stl(filepath = temp_stl_path)

print("Model loaded!")    
bpy.ops.object.origin_set(type='GEOMETRY_ORIGIN')

#Rotating the model
bpy.context.object.rotation_euler[2] = 4.71239

bpy.ops.object.editmode_toggle()

print("Cleaning bad geometry...")
#Removing double vertices
bpy.ops.mesh.remove_doubles(threshold=0.1)

bpy.ops.object.editmode_toggle()

#Set the metric units to mm
bpy.context.scene.unit_settings.system='METRIC'
bpy.context.scene.unit_settings.scale_length = 0.001

print("Smoothing model...")
#Smoothing
bpy.ops.object.modifier_add(type='SMOOTH')
bpy.context.object.modifiers["Smooth"].iterations = smooth_level
bpy.context.object.modifiers["Smooth"].factor = smooth_factor
bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Smooth")

#Apply transforms
bpy.ops.object.transform_apply(location=False, rotation=True, scale=True)
print("Saving model in: " + _stl_path)

#Save the file as .stl and .blend files
bpy.ops.wm.save_as_mainfile(filepath = _stl_path + '/Model_STL_blend.blend', copy = True)
bpy.ops.export_mesh.stl(filepath = _stl_path + '/Model_STL_smooth.stl')
print("Model was successfully saved!")
#os.remove(temp_stl_path)
