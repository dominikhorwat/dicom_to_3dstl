'''
Algorithm implemented in Python 3 to create printable, 3D STL model from CT images (cross-section).
Algorithm uses Blender 2.78 to perform model postprocessing.


version 1.0 June 1, 2017


Oryginal Authors:

Dominik Horwat, MSc., Eng.
University of Gdańsk, Faculty of Mathematics, Physics and Informatics,
Institute of Theoretical Physics and Astrophysics,
Wit Stwosz 57, 80-952 Gdańsk, horwat.dominik@gmail.com

Marek Krośnicki, PhD, Eng.	
University of Gdańsk, Faculty of Mathematics, Physics and Informatics, Institute of Theoretical Physics and Astrophysics,
Wit Stwosz 57, 80-952 Gdańsk, fizmk@univ.gda.pl, http://iftia9.univ.gda.pl/~kroch


Requirements:
- Python 3
- Python modules: natsort, numpy, numpy-stl, pydicom, scipy (see Manual)
- Blender 2.78


Algorithm structure (modules):
I.	_1DICOMAnonymizer.py
II.	_2Sorting.py
III.	_3Preprocessing.py
IV.	_4MeshCreate.py
V.	_5PostprocessingSTL.py


I. DICOMAnonymizer.py


Copyright 2017 Dominik Horwat, Marek Krośnicki


License: GPL2

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.


Description:

The function anonymizes all CT DICOM files in the list.
Folder must contains only DICOM files and cannot contains subfolders


Syntax:

[savePath] = anonymization(dicomPath, savePath)


Input:

dicomPath - a directory containing nonanonymized DICOM files
savePath - a directory to save anonimized files

Output:

savePath - path to the folder containing anonymized DICOM files

'''

import dicom as pydicom
import glob as glob
import os as os
import sys as sys
from os.path import join as join

anonstr = "None"
anonint = "0"

#The Personal data anonimization
def PN_funct(ds, data_element):
    if data_element.VR == "PN":
        data_element.value = anonstr

def DA_funct(ds, data_element):
    if data_element.VR == "DA":
        data_element.value = "rrrrmmdd"

def TM_funct(ds, data_element):
    if data_element.VR == "TM":
        data_element.value = "ggmmss"

#The anonymisation of other data
def data_funct(ds, dir):
    if ds.dir("ReferringPhysiciansAddress"):
        ds.ReferringPhysiciansAddress = anonstr
    if ds.dir("ReferringPhysiciansTelephoneNumbers"):
        ds.ReferringPhysiciansTelephoneNumbers = anonstr
    if ds.dir("ReferringPhysicianIdentificationSequence"):
        ds.ReferringPhysicianIdentificationSequence = anonstr
    if ds.dir("PhysiciansofRecordIdentification"):
        ds.PhysiciansofRecordIdentification = anonstr

    if ds.dir("PatientID"):
        ds.PatientID = anonstr
    if ds.dir("IssuerofPatientID"):
        ds.IssuerofPatientID = anonint
    if ds.dir("PatientBirthDate"):
        ds.PatientBirthDate = anonstr
    if ds.dir("PatientSex"):
        ds.PatientSex = anonstr
    if ds.dir("PatientAge"):
        ds.PatientAge = anonstr
    if ds.dir("PatientSize"):
        ds.PatientSize = anonint
    if ds.dir("PatientWeight"):
        ds.PatientWeight = "0"
    if ds.dir("PatientAddress"):
        ds.PatientAddress = anonstr
    if ds.dir("StudyID"):
        ds.StudyID = anonstr
    if ds.dir("PatientTelephoneNumber"):
        ds.PatientTelephoneNumber = anonstr
    if ds.dir("PatientComments"):
        ds.PatientComments = anonstr
    if ds.dir("PatientInstitutionResidence"):
        ds.PatientsInstitutionResidence = anonstr
    if ds.dir("OtherPatientID"):
        ds.OtherPatientIDs = anonstr

    if ds.dir("InstitutionName"):
        ds.InstitutionName = anonstr
    if ds.dir("InstitutionAddress"):
        ds.InstitutionAddress = anonstr
    if ds.dir("InstitutionCodeSequence"):
        ds.InstitutionCodeSequence = anonstr
    if ds.dir("InstitutionalDepartmentName"):
        ds.InstitutionalDepartmentName = anonstr

    if ds.dir("StationName"):
        ds.StationName = anonstr
    if ds.dir("ManufacturerModelName"):
        ds.ManufacturerModelName = anonstr
    if ds.dir("SoftwareVersions"):
        ds.SoftwareVersions = anonstr
    if ds.dir("DeviceSerialNumber"):
        ds.DeviceSerialNumber = anonstr
    
def anonymization(dicomPath, savePath):

    #Changing a default directory
    os.chdir(dicomPath)

    #Creating a path/folder for anonimized files
    #newdicomPath = join(os.path.dirname(dicomPath),'a_'+os.path.basename(dicomPath))

    #Loading DICOM files
    print("Anonymization...")
    for file in glob.glob("*"):
        tempPath = join(dicomPath, file)

        #Loading a DICOM file
        ds = pydicom.read_file(tempPath, force = True)

        #The anonymization
        ds.walk(PN_funct)
        ds.walk(DA_funct)
        ds.walk(TM_funct)
        data_funct(ds,dir)

        #Get a filename
        tempfile = os.path.basename(tempPath)
        
        #Save the file
        if not os.path.exists(savePath):
            os.makedirs(savePath)
        tempPathNew = join(savePath, tempfile)
        ds.save_as(tempPathNew)

    print("The anonymization was successfull!")
    return savePath
